<!DOCTYPE>
<html lang="ru">
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="themes/base/ui.all.css"/>
    <link rel="stylesheet" href="themes/style.css">
    <title></title>
    <script type="text/javascript" src="src/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="src/jquery.query-2.0.1.js"></script>
    <script type="text/javascript" src="src/ui.core.min.js"></script>
    <script type="text/javascript" src="src/ui.progressbar.min.js"></script>
    <script type="text/javascript" src="src/config.js"></script>
</head>
<script>
    function exportData() {
        var rows = [];

        var table = document.getElementById("tblSample");
        var averageDetails = document.getElementById("averageDetails");

        rows.push([averageDetails.innerText])
        rows.push(['Test', 'Transfer Time', 'Ping', 'Host', 'Path', 'Bytes', 'Status', 'Speed'])

        for (var i = 0, row; row = table.rows[i]; i++) {
            var push = [];
            for (var a = 0; a < row.cells.length; a++) {
                console.log(a)
                push.push(row.cells[a].innerText);

            }
            rows.push(push);
        }
        if (rows.length > 2) {
            csvContent = "data:text/csv;charset=utf-8,";
            rows.forEach(function (rowArray) {
                row = rowArray.join(",");
                csvContent += row + "\r\n";
            });

            /* create a hidden <a> DOM node and set its download attribute */
            var encodedUri = encodeURI(csvContent);
            var link = document.createElement("a");
            link.setAttribute("href", encodedUri);
            link.setAttribute("download", "Report.csv");
            document.body.appendChild(link);

            link.click();
        }
    }
</script>
<body>
<div id="mainContentFrame">
    <div class="border">
        <div id="wrapper">
            <svg id="meter">
                <circle id="outline_curves" class="circle outline" cx="50%" cy="50%">
                </circle>
                <circle id="low" class="circle range" cx="50%" cy="50%" stroke="#FDE47F">
                </circle>
                <circle id="avg" class="circle range" cx="50%" cy="50%" stroke="#7CCCE5">
                </circle>
                <circle id="high" class="circle range" cx="50%" cy="50%" stroke="#E04644">
                </circle>
                <circle id="mask" class="circle" cx="50%" cy="50%">
                </circle>
                <circle id="outline_ends" class="circle outline" cx="50%" cy="50%">
                </circle>
            </svg>
            <img id="meter_needle" src="themes/svg-meter-gauge-needle.svg" alt="">
            <label id="lbl" id="value" for="">0</label>
        </div>
        <div>
            <h3 id="title"></h3>
            <span id="subtitle"></span>
        </div>

        <div id="statusContainer">
            <div id="resultMessage">
                <h4 class="result-title"></h4>
                <div class="result-message"></div>
            </div>

            <div id="testInProgress">
                <img src="themes/loading.gif" width="16" height="16" align="absmiddle"/> <span id="testInProgressLabel"></span>
            </div>
        </div>

        <div id="progressPanel">
            <div id="progressbar"></div>
        </div>

        <div id="buttonPanel">
            <form>
                <input type="button" id="startTestButton"/>
            </form>
        </div>
        <button onclick="exportData()">
            <span class="glyphicon glyphicon-download"></span>
            Скачать результат
        </button>
    </div>
</div>
<div id="technicalDetails">
    <div id="averageDetails">
        Download speed: <span id="currentdlspeed"></span>
        &nbsp;(required minimum: <span id="requireddlspeed"></span>)
        <br/>
        Upload speed: <span id="currentulspeed"></span>
        &nbsp;(required minimum: <span id="requiredulspeed"></span>)
        <br/>
        Response time: <span id="currenttime"></span>
        &nbsp;(required maximum: <span id="requiredtime"></span>)
        <br/>
    </div>
    <table width="100%" border="0">
        <thead>
        <tr>
            <td>Test</td>
            <td>Transfer Time</td>
            <td>Ping</td>
            <td>Host</td>
            <td>Path</td>
            <td>Bytes</td>
            <td>Status</td>
            <td>Speed</td>
        </tr>
        </thead>
        <tbody id="tblSample">
        </tbody>
    </table>
</div>

<script src="src/script.js"></script>
</body>
</html>
