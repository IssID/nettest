[anderssonjohan/Nettest](https://github.com/anderssonjohan/Nettest) © github \
[hongkiat/svg-meter-gauge](https://github.com/hongkiat/svg-meter-gauge) © github

Проверка скорости от клиента до сервера (загрузка/скачивание).
---------
![nettest](https://gitlab.com/IssID/nettest/-/raw/main/example.jpg "exemple screen")

Установка без php
---------
Переименовать `index.php` в `index.html` 

Настройка для nginx
---------
Для `nginx` добавить правила отправки `post` запросов на `html` например добавив в конфиг
> error_page  405     =200 $uri;

не забудьте отключить сжатие 
> gzip off;