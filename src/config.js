var Settings = {
	Logo			: "",	// URL to logo image
	MinimumDlSpeed	: 900,			// Required download speed in kbit/s
    MinimumUlSpeed  : 300,          // Required upload speed in kbit/s
	MaximumTime		: 180,			// Required response time (milliseconds)
	RequestCount	: 80,			// Number of requests to perform
	Debug			: true			// true, if detailed output of the test run needs to be shown, otherwise false
};
var Strings = {
    StartTest		: "Начать тест",
	TestInProgress	: "Идет проверка, подождите ...",
	Title			: "Тест скорости интернет-соединения",
	SubTitle		: "Эта веб-страница позволяет вам проверить скорость вашего интернет-соединения между вашим компьютером и нашим сервером",
	TestFailed		: {
		Title	: "Проверка завершена",
		Message : "(Если это был ваш первый тест, попробуйте запустить тест еще раз через пару часов.)"
	},
	TestPassed		: {
		Title	: "Проверка завершена",
		Message : ""
	}
};

